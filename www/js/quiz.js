

function loadQuestions(){

    var idAttr = localStorage.getItem('c_id');
    var c_name = localStorage.getItem('c_name');

    db.transaction(function (tx){
        tx.executeSql('SELECT * FROM quiztbl WHERE c_id=?;',[idAttr],querysuccessfull,errorCB);
    });

    $('#selected_category1').text(c_name);
    $('#selected_category2').text(c_name);
    $('#selected_category3').text(c_name);
    $('#selected_category4').text(c_name);
    $('#selected_category5').text(c_name);
    $('#selected_category6').text(c_name);
    $('#selected_category7').text(c_name);
    $('#selected_category8').text(c_name);
    $('#selected_category9').text(c_name);
    $('#selected_category10').text(c_name);

}


function querysuccessfull(tx,result){
    var len = result.rows.length;
    var resultArray = [];

    for(var i=0; i < result.rows.length; i+=1) {
        resultArray.push(result.rows.item(i));
    }

    var shuffledArray = shuffle(resultArray);

    for(var i=0; i < 10 ; i++){
        var questArr = new Array();

        questArr[0] = shuffledArray[i].question;
        questArr[1] = shuffledArray[i].qanswer1;
        questArr[2] = shuffledArray[i].qanswer2;
        questArr[3] = shuffledArray[i].qanswer3;
        questArr[4] = shuffledArray[i].qanswer4;
        questArr[5] = shuffledArray[i].answer;
        gameArr[i] = questArr;
    }

    showquestion();
};



function showquestion(){
    console.log('showquestion');

    $("#question1").empty();
    $("#question2").empty();
    $("#question3").empty();
    $("#question4").empty();
    $("#question5").empty();
    $("#question6").empty();
    $("#question7").empty();
    $("#question8").empty();
    $("#question9").empty();
    $("#question10").empty();


    $('#answer1-1').empty();
    $('#answer1-2').empty();
    $('#answer1-3').empty();
    $('#answer1-4').empty();

    $('#answer2-1').empty();
    $('#answer2-2').empty();
    $('#answer2-3').empty();
    $('#answer2-4').empty();

    $('#answer3-1').empty();
    $('#answer3-2').empty();
    $('#answer3-3').empty();
    $('#answer3-4').empty();

    $('#answer4-1').empty();
    $('#answer4-2').empty();
    $('#answer4-3').empty();
    $('#answer4-4').empty();

    $('#answer5-1').empty();
    $('#answer5-2').empty();
    $('#answer5-3').empty();
    $('#answer5-4').empty();

    $('#answer6-1').empty();
    $('#answer6-2').empty();
    $('#answer6-3').empty();
    $('#answer6-4').empty();

    $('#answer7-1').empty();
    $('#answer7-2').empty();
    $('#answer7-3').empty();
    $('#answer7-4').empty();

    $('#answer8-1').empty();
    $('#answer8-2').empty();
    $('#answer8-3').empty();
    $('#answer8-4').empty();

    $('#answer9-1').empty();
    $('#answer9-2').empty();
    $('#answer9-3').empty();
    $('#answer9-4').empty();

    $('#answer10-1').empty();
    $('#answer10-2').empty();
    $('#answer10-3').empty();
    $('#answer10-4').empty();



        indexquestion = Math.floor((Math.random()*5));
        var d = 0;
        var vitri = Math.ceil((Math.random()*4));
        var vitri1 = Math.ceil((Math.random()*4));
        var vitri2 = Math.ceil((Math.random()*4));
        do{
            vitri = Math.ceil((Math.random()*4));
            vitri1 = Math.ceil((Math.random()*4));
            vitri2 = Math.ceil((Math.random()*4));
            vitri3 = Math.ceil((Math.random()*4));

            if(vitri != vitri1 && vitri != vitri2 && vitri1 != vitri2 && vitri !=vitri3 && vitri1 != vitri3 && vitri2 != vitri3){
                d =1;

                $("#question1").text('1.) ' + gameArr[0][0]);
                $('#answer1-1').text(gameArr[0][vitri]);
                $('#answer1-2').text(gameArr[0][vitri1]);
                $('#answer1-3').text(gameArr[0][vitri2]);
                $('#answer1-4').text(gameArr[0][vitri3]);

                $("#question2").text('2.) ' + gameArr[1][0]);
                $('#answer2-1').text(gameArr[1][vitri]);
                $('#answer2-2').text(gameArr[1][vitri1]);
                $('#answer2-3').text(gameArr[1][vitri2]);
                $('#answer2-4').text(gameArr[1][vitri3]);

                $("#question3").text('3.) ' + gameArr[2][0]);
                $('#answer3-1').text(gameArr[2][vitri]);
                $('#answer3-2').text(gameArr[2][vitri1]);
                $('#answer3-3').text(gameArr[2][vitri2]);
                $('#answer3-4').text(gameArr[2][vitri3]);

                $("#question4").text('4.) ' + gameArr[3][0]);
                $('#answer4-1').text(gameArr[3][vitri]);
                $('#answer4-2').text(gameArr[3][vitri1]);
                $('#answer4-3').text(gameArr[3][vitri2]);
                $('#answer4-4').text(gameArr[3][vitri3]);

                $("#question5").text('5.) ' + gameArr[4][0]);
                $('#answer5-1').text(gameArr[4][vitri]);
                $('#answer5-2').text(gameArr[4][vitri1]);
                $('#answer5-3').text(gameArr[4][vitri2]);
                $('#answer5-4').text(gameArr[4][vitri3]);

                $("#question6").text('6.) ' + gameArr[5][0]);
                $('#answer6-1').text(gameArr[5][vitri]);
                $('#answer6-2').text(gameArr[5][vitri1]);
                $('#answer6-3').text(gameArr[5][vitri2]);
                $('#answer6-4').text(gameArr[5][vitri3]);

                $("#question7").text('7.) ' + gameArr[6][0]);
                $('#answer7-1').text(gameArr[6][vitri]);
                $('#answer7-2').text(gameArr[6][vitri1]);
                $('#answer7-3').text(gameArr[6][vitri2]);
                $('#answer7-4').text(gameArr[6][vitri3]);

                $("#question8").text('8.) ' + gameArr[7][0]);
                $('#answer8-1').text(gameArr[7][vitri]);
                $('#answer8-2').text(gameArr[7][vitri1]);
                $('#answer8-3').text(gameArr[7][vitri2]);
                $('#answer8-4').text(gameArr[7][vitri3]);

                $("#question9").text('9.) ' + gameArr[8][0]);
                $('#answer9-1').text(gameArr[8][vitri]);
                $('#answer9-2').text(gameArr[8][vitri1]);
                $('#answer9-3').text(gameArr[8][vitri2]);
                $('#answer9-4').text(gameArr[8][vitri3]);

                $("#question10s").text('10.) ' + gameArr[9][0]);
                $('#answer10-1').text(gameArr[9][vitri]);
                $('#answer10-2').text(gameArr[9][vitri1]);
                $('#answer10-3').text(gameArr[9][vitri2]);
                $('#answer10-4').text(gameArr[9][vitri3]);


                questionArray.push(gameArr);
                console.log(gameArr);

            }
        }while(d == 0);
};


$( document ).on("pageinit", "#quiz1", function() {
      
    $( "#btn1_1" ).click(function( event ) {
        var answer = $('#answer1-1').text();
        var trueanswer = gameArr[0][5];

       
        console.log('1. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');

        setInterval(function() {
            if ($('#btn1_1').hasClass("ui-btn-a")){
                 $('#btn1_1').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn1_1').hasClass("ui-btn-g")){
                 $('#btn1_1').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1; 

                //playAudio('/android_asset/www/music/correct.mp3');

                if ($('#btn1_1').hasClass("ui-btn-a")){
                     $('#btn1_1').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn1_1').hasClass("ui-btn-g")){
                     $('#btn1_1').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    // $.mobile.changePage($("#quiz2"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    // window.location = "quiz.html#quiz2";
                    $.mobile.pageContainer.pagecontainer("change", "#quiz2");
                    return false;
                }, 1000);

            }else{        

                //playAudio('/android_asset/www/music/false.mp3');

                if ($('#btn1_1').hasClass("ui-btn-a")){
                     $('#btn1_1').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn1_1').hasClass("ui-btn-g")){
                     $('#btn1_1').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }

                }else{
                    setTimeout(function(){
                        $.mobile.changePage($("#quiz2"), { transition: "none" });
                        event.preventDefault();
                        return false;
                    }, 2000);
                }

            }
        }, 3000);
    });

    $( "#btn1_2" ).click(function( event ) {
        var answer = $('#answer1-2').text();
        var trueanswer = gameArr[0][5];

        console.log('1. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);

        //playAudio('/android_asset/www/music/take_chq.mp3');

        setInterval(function() {
            if ($('#btn1_2').hasClass("ui-btn-a")){
                 $('#btn1_2').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn1_2').hasClass("ui-btn-g")){
                 $('#btn1_2').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);


        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1; 
                
                //playAudio('/android_asset/www/music/correct.mp3');

                if ($('#btn1_2').hasClass("ui-btn-a")){
                     $('#btn1_2').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn1_2').hasClass("ui-btn-g")){
                     $('#btn1_2').removeClass("ui-btn-g").addClass("ui-btn-e");
                }            
                
                progress = progress + 1;

                setTimeout(function(){
                    // $.mobile.changePage($("#quiz2"), { transition: "none" });
                    // event.preventDefault();
                    event.stopPropagation();
                    event.preventDefault();
                    // window.location = "quiz.html#quiz2";
                    $.mobile.pageContainer.pagecontainer("change", "#quiz2");
                    return false;
                }, 1000);

            }else{        
                
                //playAudio('/android_asset/www/music/false.mp3');

                if ($('#btn1_2').hasClass("ui-btn-a")){
                     $('#btn1_2').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn1_2').hasClass("ui-btn-g")){
                     $('#btn1_2').removeClass("ui-btn-g").addClass("ui-btn-d");
                } 
                
                
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }

                }else{
                     setTimeout(function(){
                        $.mobile.changePage($("#quiz2"), { transition: "none" });
                        event.preventDefault();
                        return false;
                    }, 2000);
                }


            }
        }, 3000);
    });

    $( "#btn1_3" ).click(function( event ) {
        var answer = $('#answer1-3').text();
        var trueanswer = gameArr[0][5];

        console.log('1. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);

        //playAudio('/android_asset/www/music/take_chq.mp3');

        setInterval(function() {
            if ($('#btn1_3').hasClass("ui-btn-a")){
                 $('#btn1_3').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn1_3').hasClass("ui-btn-g")){
                 $('#btn1_3').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);


        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1; 

                //playAudio('/android_asset/www/music/correct.mp3');

                if ($('#btn1_3').hasClass("ui-btn-a")){
                     $('#btn1_3').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn1_3').hasClass("ui-btn-g")){
                     $('#btn1_3').removeClass("ui-btn-g").addClass("ui-btn-e");
                } 
                
                progress = progress + 1;
                setTimeout(function(){
                    // $.mobile.changePage($("#quiz2"), { transition: "none" });
                    // event.preventDefault();
                    event.stopPropagation();
                    event.preventDefault();
                    // window.location = "quiz.html#quiz2";
                    $.mobile.pageContainer.pagecontainer("change", "#quiz2");
                    return false;
                }, 1000);

            }else{

                //playAudio('/android_asset/www/music/false.mp3');

                if ($('#btn1_3').hasClass("ui-btn-a")){
                     $('#btn1_3').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn1_3').hasClass("ui-btn-g")){
                     $('#btn1_3').removeClass("ui-btn-g").addClass("ui-btn-d");
                } 
                
                
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        $.mobile.changePage($("#quiz2"), { transition: "none" });
                        event.preventDefault();
                        return false;
                    }, 2000);
                }

            }
        }, 3000);
    });

    $( "#btn1_4" ).click(function( event ) {
        var answer = $('#answer1-4').text();
        var trueanswer = gameArr[0][5];

        console.log('1. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);

        //playAudio('/android_asset/www/music/take_chq.mp3');

        setInterval(function() {
            if ($('#btn1_4').hasClass("ui-btn-a")){
                 $('#btn1_4').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn1_4').hasClass("ui-btn-g")){
                 $('#btn1_4').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1; 

                //playAudio('/android_asset/www/music/correct.mp3');

                if ($('#btn1_4').hasClass("ui-btn-a")){
                     $('#btn1_4').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn1_4').hasClass("ui-btn-g")){
                     $('#btn1_4').removeClass("ui-btn-g").addClass("ui-btn-e");
                }             
                
                progress = progress + 1;

                setTimeout(function(){
                    // $.mobile.changePage($("#quiz2"), { transition: "none" });
                    // event.preventDefault();
                    event.stopPropagation();
                    event.preventDefault();
                    // window.location = "quiz.html#quiz2";
                    $.mobile.pageContainer.pagecontainer("change", "#quiz2");
                    return false;
                }, 1000);

            }else{

                //playAudio('/android_asset/www/music/false.mp3');

                if ($('#btn1_4').hasClass("ui-btn-a")){
                     $('#btn1_4').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn1_4').hasClass("ui-btn-g")){
                     $('#btn1_4').removeClass("ui-btn-g").addClass("ui-btn-d");
                } 
                
                
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.changePage($("#quiz2"), { transition: "none" });
                        return false;
                    }, 2000);
                }

            }
        }, 3000);
    });
});



$( document ).on("pageinit", "#quiz2", function() {

    $( "#btn2_1" ).click(function( event ) {
        var answer = $('#answer2-1').text();
        var trueanswer = gameArr[1][5];

        console.log('2. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);

        //playAudio('/android_asset/www/music/take_chq.mp3');

        setInterval(function() {
            if ($('#btn2_1').hasClass("ui-btn-a")){
                 $('#btn2_1').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn2_1').hasClass("ui-btn-g")){
                 $('#btn2_1').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;

                //playAudio('/android_asset/www/music/correct.mp3');

                if ($('#btn2_1').hasClass("ui-btn-a")){
                    $('#btn2_1').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn2_1').hasClass("ui-btn-g")){
                    $('#btn2_1').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1; 
                
                setTimeout(function(){
                    // $.mobile.changePage($("#quiz3"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    $.mobile.pageContainer.pagecontainer("change", "#quiz3");
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn2_1').hasClass("ui-btn-a")){
                    $('#btn2_1').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn2_1').hasClass("ui-btn-g")){
                    $('#btn2_1').removeClass("ui-btn-g").addClass("ui-btn-d");
                } 

                
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                     if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }

                }else{
                    setTimeout(function(){
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question8");
                        return false;
                    }, 2000);
                }

            }
        }, 3000);
    });

    $( "#btn2_2" ).click(function( event ) {
        var answer = $('#answer2-2').text();
        var trueanswer = gameArr[1][5];

        console.log('2. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        
        //playAudio('/android_asset/www/music/take_chq.mp3');

        setInterval(function() {
            if ($('#btn2_2').hasClass("ui-btn-a")){
                 $('#btn2_2').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn2_2').hasClass("ui-btn-g")){
                 $('#btn2_2').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn2_2').hasClass("ui-btn-a")){
                    $('#btn2_2').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn2_2').hasClass("ui-btn-g")){
                    $('#btn2_2').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;
                
                setTimeout(function(){
                    // $.mobile.changePage($("#quiz3"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    $.mobile.pageContainer.pagecontainer("change", "#quiz3");
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn2_2').hasClass("ui-btn-a")){
                    $('#btn2_2').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn2_2').hasClass("ui-btn-g")){
                    $('#btn2_2').removeClass("ui-btn-g").addClass("ui-btn-d");
                } 
                
                
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#quiz3");
                        return false;
                    }, 2000);
                }

            }
        }, 3000);
    });

    $( "#btn2_3" ).click(function( event ) {
        var answer = $('#answer2-3').text();
        var trueanswer = gameArr[1][5];

        console.log('2. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);

        //playAudio('/android_asset/www/music/take_chq.mp3');

        setInterval(function() {
            if ($('#btn2_3').hasClass("ui-btn-a")){
                 $('#btn2_3').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn2_3').hasClass("ui-btn-g")){
                 $('#btn2_3').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn2_3').hasClass("ui-btn-a")){
                    $('#btn2_3').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn2_3').hasClass("ui-btn-g")){
                    $('#btn2_3').removeClass("ui-btn-g").addClass("ui-btn-e");
                } 

                progress = progress + 1;

                setTimeout(function(){
                    // $.mobile.changePage($("#quiz3"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    $.mobile.pageContainer.pagecontainer("change", "#quiz3");
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn2_3').hasClass("ui-btn-a")){
                    $('#btn2_3').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn2_3').hasClass("ui-btn-g")){
                    $('#btn2_3').removeClass("ui-btn-g").addClass("ui-btn-d");
                }
                
                
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#quiz3"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        // window.location = "quiz.html#quiz3";
                        $.mobile.pageContainer.pagecontainer("change", "#quiz3");
                        return false;
                    }, 2000);
                }

            }
        }, 3000);
    });

    $( "#btn2_4" ).click(function( event ) {
        var answer = $('#answer2-4').text();
        var trueanswer = gameArr[1][5];

        console.log('2. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn2_4').hasClass("ui-btn-a")){
                 $('#btn2_4').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn2_4').hasClass("ui-btn-g")){
                 $('#btn2_4').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn2_4').hasClass("ui-btn-a")){
                    $('#btn2_4').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn2_4').hasClass("ui-btn-g")){
                    $('#btn2_4').removeClass("ui-btn-g").addClass("ui-btn-e");
                }         

                progress = progress + 1;

                setTimeout(function(){
                    // $.mobile.changePage($("#quiz3"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    // window.location = "quiz.html#quiz3";
                    $.mobile.pageContainer.pagecontainer("change", "#quiz3");
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn2_4').hasClass("ui-btn-a")){
                    $('#btn2_4').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn2_4').hasClass("ui-btn-g")){
                    $('#btn2_4').removeClass("ui-btn-g").addClass("ui-btn-d");
                }     
                
                
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#quiz3"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        // window.location = "quiz.html#quiz3";
                        $.mobile.pageContainer.pagecontainer("change", "#quiz3");
                    }, 2000);
                }

            }
        }, 3000);
    });
});



$( document ).on("pageinit", "#quiz3", function() {
    $( "#btn3_1" ).click(function( event ) {
        var answer = $('#answer3-1').text();
        var trueanswer = gameArr[2][5];

        console.log('3. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        
        //playAudio('/android_asset/www/music/take_chq.mp3');

        setInterval(function() {
            if ($('#btn3_1').hasClass("ui-btn-a")){
                 $('#btn3_1').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn3_1').hasClass("ui-btn-g")){
                 $('#btn3_1').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn3_1').hasClass("ui-btn-a")){
                    $('#btn3_1').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn3_1').hasClass("ui-btn-g")){
                    $('#btn3_1').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    // $.mobile.changePage($("#quiz4"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    // window.location = "quiz.html#quiz4";
                    $.mobile.pageContainer.pagecontainer("change", "#quiz4");
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn3_1').hasClass("ui-btn-a")){
                    $('#btn3_1').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn3_1').hasClass("ui-btn-g")){
                    $('#btn3_1').removeClass("ui-btn-g").addClass("ui-btn-d");
                }
                
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#quiz4"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        // window.location = "quiz.html#quiz4";
                        $.mobile.pageContainer.pagecontainer("change", "#quiz4");
                    }, 2000);
                }

            }
        }, 3000);
    });

    $( "#btn3_2" ).click(function( event ) {
        var answer = $('#answer3-2').text();
        var trueanswer = gameArr[2][5];

        console.log('3. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn3_2').hasClass("ui-btn-a")){
                 $('#btn3_2').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn3_2').hasClass("ui-btn-g")){
                 $('#btn3_2').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer + 1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn3_2').hasClass("ui-btn-a")){
                    $('#btn3_2').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn3_2').hasClass("ui-btn-g")){
                    $('#btn3_2').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    // $.mobile.changePage($("#quiz4"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    // window.location = "quiz.html#quiz4";
                    $.mobile.pageContainer.pagecontainer("change", "#quiz4");
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn3_2').hasClass("ui-btn-a")){
                    $('#btn3_2').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn3_2').hasClass("ui-btn-g")){
                    $('#btn3_2').removeClass("ui-btn-g").addClass("ui-btn-d");
                }
                
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }

                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#quiz4"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        // window.location = "quiz.html#quiz4";
                        $.mobile.pageContainer.pagecontainer("change", "#quiz4");
                        return false;
                    }, 2000);
                }

            }
        }, 3000);
    });

    $( "#btn3_3" ).click(function( event ) {
        var answer = $('#answer3-3').text();
        var trueanswer = gameArr[2][5];

        console.log('3. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn3_3').hasClass("ui-btn-a")){
                 $('#btn3_3').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn3_3').hasClass("ui-btn-g")){
                 $('#btn3_3').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);    

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1; 
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn3_3').hasClass("ui-btn-a")){
                    $('#btn3_3').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn3_3').hasClass("ui-btn-g")){
                    $('#btn3_3').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;
                
                setTimeout(function(){
                    // $.mobile.changePage($("#quiz4"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    // window.location = "quiz.html#quiz4";
                    $.mobile.pageContainer.pagecontainer("change", "#quiz4");
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn3_3').hasClass("ui-btn-a")){
                    $('#btn3_3').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn3_3').hasClass("ui-btn-g")){
                    $('#btn3_3').removeClass("ui-btn-g").addClass("ui-btn-d");
                }
                
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#quiz4"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        window.location = "quiz.html#quiz4";
                        return false;
                    }, 2000);
                }

            }
        }, 3000);
    });

    $( "#btn3_4" ).click(function( event ) {
        var answer = $('#answer3-4').text();
        var trueanswer = gameArr[2][5];

        console.log('3. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn3_4').hasClass("ui-btn-a")){
                 $('#btn3_4').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn3_4').hasClass("ui-btn-g")){
                 $('#btn3_4').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);  

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn3_4').hasClass("ui-btn-a")){
                    $('#btn3_4').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn3_4').hasClass("ui-btn-g")){
                    $('#btn3_4').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    // $.mobile.changePage($("#quiz4"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    // window.location = "quiz.html#quiz4";
                    $.mobile.pageContainer.pagecontainer("change", "#quiz4");
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn3_4').hasClass("ui-btn-a")){
                    $('#btn3_4').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn3_4').hasClass("ui-btn-g")){
                    $('#btn3_4').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#quiz4"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        // window.location = "quiz.html#quiz4";
                        $.mobile.pageContainer.pagecontainer("change", "#quiz4");
                        return false;
                    }, 2000);
                }

            }
        }, 3000);
    });
});



$( document ).on("pageinit", "#quiz4", function() {
    $( "#btn4_1" ).click(function( event ) {
        var answer = $('#answer4-1').text();
        var trueanswer = gameArr[3][5];

        console.log('4. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn4_1').hasClass("ui-btn-a")){
                 $('#btn4_1').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn4_1').hasClass("ui-btn-g")){
                 $('#btn4_1').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);  

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn4_1').hasClass("ui-btn-a")){
                    $('#btn4_1').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn4_1').hasClass("ui-btn-g")){
                    $('#btn4_1').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    // $.mobile.changePage($("#quiz5"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    $.mobile.pageContainer.pagecontainer("change", "#quiz5");
                    return false;
                }, 2000);
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn4_1').hasClass("ui-btn-a")){
                    $('#btn4_1').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn4_1').hasClass("ui-btn-g")){
                    $('#btn4_1').removeClass("ui-btn-g").addClass("ui-btn-d");
                }
                
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#quiz5"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#quiz5");
                        return false;
                    }, 2000);
                }

            }
        }, 3000);
    });

    $( "#btn4_2" ).click(function( event ) {
        var answer = $('#answer4-2').text();
        var trueanswer = gameArr[3][5];

        console.log('4. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn4_2').hasClass("ui-btn-a")){
                 $('#btn4_2').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn4_2').hasClass("ui-btn-g")){
                 $('#btn4_2').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);  

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1; 
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn4_2').hasClass("ui-btn-a")){
                    $('#btn4_2').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn4_2').hasClass("ui-btn-g")){
                    $('#btn4_2').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    // $.mobile.changePage($("#quiz5"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    $.mobile.pageContainer.pagecontainer("change", "#quiz5");
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn4_2').hasClass("ui-btn-a")){
                    $('#btn4_2').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn4_2').hasClass("ui-btn-g")){
                    $('#btn4_2').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#quiz5"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#quiz5");
                        return false;
                    }, 2000);
                }

            }
        }, 3000);
    });

    $( "#btn4_3" ).click(function( event ) {
        var answer = $('#answer4-3').text();
        var trueanswer = gameArr[3][5];

        console.log('4. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn4_3').hasClass("ui-btn-a")){
                 $('#btn4_3').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn4_3').hasClass("ui-btn-g")){
                 $('#btn4_3').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn4_3').hasClass("ui-btn-a")){
                     $('#btn4_3').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn4_3').hasClass("ui-btn-g")){
                     $('#btn4_3').removeClass("ui-btn-g").addClass("ui-btn-e");
                }    

                progress = progress + 1;

                setTimeout(function(){
                    // $.mobile.changePage($("#quiz5"));
                    event.stopPropagation();
                    event.preventDefault();
                    $.mobile.pageContainer.pagecontainer("change", "#quiz5");
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn4_3').hasClass("ui-btn-a")){
                     $('#btn4_3').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn4_3').hasClass("ui-btn-g")){
                     $('#btn4_3').removeClass("ui-btn-g").addClass("ui-btn-d");
                }    
                
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#quiz5"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#quiz5");
                        return false;
                    }, 2000);
                }

            }
        }, 3000);    
    });

    $( "#btn4_4" ).click(function( event ) {
        var answer = $('#answer4-4').text();
        var trueanswer = gameArr[3][5];

        console.log('4. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn4_4').hasClass("ui-btn-a")){
                 $('#btn4_4').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn4_4').hasClass("ui-btn-g")){
                 $('#btn4_4').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1; 
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn4_4').hasClass("ui-btn-a")){
                    $('#btn4_4').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn4_4').hasClass("ui-btn-g")){
                    $('#btn4_4').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;
                
                setTimeout(function(){
                    // $.mobile.changePage($("#quiz5"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    $.mobile.pageContainer.pagecontainer("change", "#quiz5");
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn4_4').hasClass("ui-btn-a")){
                    $('#btn4_4').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn4_4').hasClass("ui-btn-g")){
                    $('#btn4_4').removeClass("ui-btn-g").addClass("ui-btn-d");
                }
                
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#quiz5"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#quiz5");
                        return false;
                    }, 2000);
                }

            }
        }, 3000);
    });
});


$( document ).on("pageinit", "#quiz5", function() {

    $( "#btn5_1" ).click(function( event ) {
        var answer = $('#answer5-1').text();
        var trueanswer = gameArr[4][5];

        console.log('5. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn5_1').hasClass("ui-btn-a")){
                 $('#btn5_1').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn5_1').hasClass("ui-btn-g")){
                 $('#btn5_1').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn5_1').hasClass("ui-btn-a")){
                     $('#btn5_1').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn5_1').hasClass("ui-btn-g")){
                     $('#btn5_1').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    // $.mobile.pageContainer.pagecontainer("change", "#question6");
                    $.mobile.changePage($("#question6s"), { transition: "none" });
                    // window.location.href="#question6";
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn5_1').hasClass("ui-btn-a")){
                     $('#btn5_1').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn5_1').hasClass("ui-btn-g")){
                     $('#btn5_1').removeClass("ui-btn-g").addClass("ui-btn-d");
                }
                
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#question6"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question6s");
                        return false;
                    }, 2000);
                }

            }
        }, 3000);
    });  

    $( "#btn5_2" ).click(function( event ) {
        var answer = $('#answer5-2').text();
        var trueanswer = gameArr[4][5];

        console.log('5. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn5_2').hasClass("ui-btn-a")){
                 $('#btn5_2').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn5_2').hasClass("ui-btn-g")){
                 $('#btn5_2').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn5_2').hasClass("ui-btn-a")){
                    $('#btn5_2').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn5_2').hasClass("ui-btn-g")){
                     $('#btn5_2').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    // $.mobile.pageContainer.pagecontainer("change", "#question6");
                    $.mobile.changePage($("#question6s"), { transition: "none" });
                    // window.location.href="#question6";
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn5_2').hasClass("ui-btn-a")){
                    $('#btn5_2').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn5_2').hasClass("ui-btn-g")){
                     $('#btn5_2').removeClass("ui-btn-g").addClass("ui-btn-d");
                }
                
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#question6"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question6s");
                        return false;
                    }, 2000);
                }

            }
        }, 3000);
    });

    $( "#btn5_3" ).click(function( event ) {
        var answer = $('#answer5-3').text();
        var trueanswer = gameArr[4][5];

        console.log('5. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn5_3').hasClass("ui-btn-a")){
                 $('#btn5_3').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn5_3').hasClass("ui-btn-g")){
                 $('#btn5_3').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);


        setTimeout(function(){
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn5_3').hasClass("ui-btn-a")){
                     $('#btn5_3').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn5_3').hasClass("ui-btn-g")){
                     $('#btn5_3').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    
                    // $.mobile.pageContainer.pagecontainer("change", "#question6");
                    $.mobile.changePage($("#question6s"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }, 2000);
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn5_3').hasClass("ui-btn-a")){
                     $('#btn5_3').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn5_3').hasClass("ui-btn-g")){
                     $('#btn5_3').removeClass("ui-btn-g").addClass("ui-btn-d");
                }
                    
                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#question6"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question6s");
                        return false;
                    }, 2000);
                }

            }
        }, 3000);
    });

    $( "#btn5_4" ).click(function( event ) {
        var answer = $('#answer5-4').text();
        var trueanswer = gameArr[4][5];

        console.log('5. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn5_4').hasClass("ui-btn-a")){
                 $('#btn5_4').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn5_4').hasClass("ui-btn-g")){
                 $('#btn5_4').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn5_4').hasClass("ui-btn-a")){
                     $('#btn5_4').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn5_4').hasClass("ui-btn-g")){
                     $('#btn5_4').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    $.mobile.changePage($("#question6s"), { transition: "none" });
                    // $.mobile.pageContainer.pagecontainer("change", "#question6");
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }, 2000);
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn5_4').hasClass("ui-btn-a")){
                     $('#btn5_4').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn5_4').hasClass("ui-btn-g")){
                     $('#btn5_4').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#question6"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question6s");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
    });
});


$( document ).on("pageinit", "#question6s", function() {
    
    $( "#btn6_1" ).click(function( event ) {
        var answer = $('#answer6-1').text();
        var trueanswer = gameArr[5][5];

        console.log('7. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn6_1').hasClass("ui-btn-a")){
                 $('#btn6_1').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn6_1').hasClass("ui-btn-g")){
                 $('#btn6_1').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn6_1').hasClass("ui-btn-a")){
                     $('#btn6_1').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn6_1').hasClass("ui-btn-g")){
                     $('#btn6_1').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    event.stopPropagation();
                    event.preventDefault();
                    // $.mobile.changePage($("#question7"), { transition: "none" });
                    // window.location.href="quiz.html#question7";
                    $.mobile.pageContainer.pagecontainer("change", "#question7s");
                    return false;
                }, 2000);
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn6_1').hasClass("ui-btn-a")){
                     $('#btn6_1').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn6_1').hasClass("ui-btn-g")){
                     $('#btn6_1').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#question7"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question7s");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });

    $( "#btn6_2" ).click(function( event ) {
        var answer = $('#answer6-2').text();
        var trueanswer = gameArr[5][5];

        console.log('6. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn6_2').hasClass("ui-btn-a")){
                 $('#btn6_2').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn6_2').hasClass("ui-btn-g")){
                 $('#btn6_2').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn6_2').hasClass("ui-btn-a")){
                     $('#btn6_2').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn6_2').hasClass("ui-btn-g")){
                     $('#btn6_2').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){

                    // $.mobile.changePage($("#question7"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    // window.location.href="quiz.html#question7";
                    $.mobile.pageContainer.pagecontainer("change", "#question7s");
                    return false;
                }, 2000);
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn6_2').hasClass("ui-btn-a")){
                     $('#btn6_2').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn6_2').hasClass("ui-btn-g")){
                     $('#btn6_2').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#question7"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question7s");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });

    $( "#btn6_3" ).click(function( event ) {
        var answer = $('#answer6-3').text();
        var trueanswer = gameArr[5][5];

        console.log('6. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn6_3').hasClass("ui-btn-a")){
                 $('#btn6_3').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn6_3').hasClass("ui-btn-g")){
                 $('#btn6_3').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn6_3').hasClass("ui-btn-a")){
                    $('#btn6_3').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn6_3').hasClass("ui-btn-g")){
                    $('#btn6_3').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    event.stopPropagation();
                    event.preventDefault();
                    // $.mobile.changePage($("#question7"), { transition: "none" });
                    // window.location.href="quiz.html#question7";
                    $.mobile.pageContainer.pagecontainer("change", "#question7s");
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn6_3').hasClass("ui-btn-a")){
                     $('#btn6_3').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn6_3').hasClass("ui-btn-g")){
                     $('#btn6_3').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    };
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#question7"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question7s");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });

    $( "#btn6_4" ).click(function( event ) {
        var answer = $('#answer6-4').text();
        var trueanswer = gameArr[5][5];

        console.log('6. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn6_4').hasClass("ui-btn-a")){
                 $('#btn6_4').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn6_4').hasClass("ui-btn-g")){
                 $('#btn6_4').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn6_4').hasClass("ui-btn-a")){
                    $('#btn6_4').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn6_4').hasClass("ui-btn-g")){
                    $('#btn6_4').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    event.stopPropagation();
                    event.preventDefault();
                    // $.mobile.changePage($("#question7"), { transition: "none" });
                    // window.location.href="quiz.html#question7";
                    $.mobile.pageContainer.pagecontainer("change", "#question7s");
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn6_4').hasClass("ui-btn-a")){
                     $('#btn6_4').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn6_4').hasClass("ui-btn-g")){
                     $('#btn6_4').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#question7"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question7s");
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });
});


$( document ).on("pageinit", "#question7s", function() {

    $( "#btn7_1" ).click(function( event ) {
        var answer = $('#answer7-1').text();
        var trueanswer = gameArr[6][5];

        console.log('7 ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn7_1').hasClass("ui-btn-a")){
                 $('#btn7_1').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn7_1').hasClass("ui-btn-g")){
                 $('#btn7_1').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn7_1').hasClass("ui-btn-a")){
                     $('#btn7_1').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn7_1').hasClass("ui-btn-g")){
                     $('#btn7_1').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    //$.mobile.changePage($("#question8"), { transition: "none" });
                    window.location="quiz.html#question8s";
                    // $.mobile.pageContainer.pagecontainer("change", "#question8");
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }, 2000);
            
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn7_1').hasClass("ui-btn-a")){
                     $('#btn7_1').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn7_1').hasClass("ui-btn-g")){
                     $('#btn7_1').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                                    event.stopPropagation();
                                    event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                                event.stopPropagation();
                                event.preventDefault();
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question8s");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });

    $( "#btn7_2" ).click(function( event ) {
        var answer = $('#answer7-2').text();
        var trueanswer = gameArr[6][5];

        console.log('7. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn7_2').hasClass("ui-btn-a")){
                 $('#btn7_2').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn7_2').hasClass("ui-btn-g")){
                 $('#btn7_2').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn7_2').hasClass("ui-btn-a")){
                     $('#btn7_2').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn7_2').hasClass("ui-btn-g")){
                     $('#btn7_2').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    
                    // $.mobile.changePage($("#question8"), { transition: "none" });
                    window.location="quiz.html#question8s";
                    // $.mobile.pageContainer.pagecontainer("change", "#question8");
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn7_2').hasClass("ui-btn-a")){
                     $('#btn7_2').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn7_2').hasClass("ui-btn-g")){
                     $('#btn7_2').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                                event.stopPropagation();
                                event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                                event.stopPropagation();
                                event.preventDefault();
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                            event.stopPropagation();
                            event.preventDefault();
                            $.mobile.pageContainer.pagecontainer("change", "#question8s");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });

    $( "#btn7_3" ).click(function( event ) {
        var answer = $('#answer7-3').text();
        var trueanswer = gameArr[6][5];

        console.log('7. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn7_3').hasClass("ui-btn-a")){
                 $('#btn7_3').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn7_3').hasClass("ui-btn-g")){
                 $('#btn7_3').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn7_3').hasClass("ui-btn-a")){
                    $('#btn7_3').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn7_3').hasClass("ui-btn-g")){
                    $('#btn7_3').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    
                    // $.mobile.changePage($("#question8"), { transition: "none" });
                    window.location="quiz.html#question8s";
                    // $.mobile.pageContainer.pagecontainer("change", "#question8");
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }, 2000);
                
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn7_3').hasClass("ui-btn-a")){
                     $('#btn7_3').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn7_3').hasClass("ui-btn-g")){
                     $('#btn7_3').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    };
                }else{
                    setTimeout(function(){
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question8s");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });

    $( "#btn7_4" ).click(function( event ) {
        var answer = $('#answer7-4').text();
        var trueanswer = gameArr[6][5];

        console.log('7. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn7_4').hasClass("ui-btn-a")){
                 $('#btn7_4').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn7_4').hasClass("ui-btn-g")){
                 $('#btn7_4').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn7_4').hasClass("ui-btn-a")){
                    $('#btn7_4').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn7_4').hasClass("ui-btn-g")){
                    $('#btn7_4').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    
                    // $.mobile.changePage($("#question8"), { transition: "none" });
                    window.location="quiz.html#question8s";
                    // $.mobile.pageContainer.pagecontainer("change", "#question8");
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn6_4').hasClass("ui-btn-a")){
                     $('#btn6_4').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn6_4').hasClass("ui-btn-g")){
                     $('#btn6_4').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question8s");
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });
});


$( document ).on("pageinit", "#question8s", function() {

    $( "#btn8_1" ).click(function( event ) {
        var answer = $('#answer8-1').text();
        var trueanswer = gameArr[7][5];

        console.log('8 ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn8_1').hasClass("ui-btn-a")){
                 $('#btn8_1').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn8_1').hasClass("ui-btn-g")){
                 $('#btn8_1').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn8_1').hasClass("ui-btn-a")){
                     $('#btn8_1').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn8_1').hasClass("ui-btn-g")){
                     $('#btn8_1').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    event.stopPropagation();
                    event.preventDefault();
                    // window.location = "quiz.html#question9"
                    $.mobile.pageContainer.pagecontainer("change", "#question9s");
                    // $.mobile.changePage($("#question9"), { transition: "none" });
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn8_1').hasClass("ui-btn-a")){
                     $('#btn8_1').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn8_1').hasClass("ui-btn-g")){
                     $('#btn8_1').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#question9"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question9s");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });

    $( "#btn8_2" ).click(function( event ) {
        var answer = $('#answer8-2').text();
        var trueanswer = gameArr[7][5];

        console.log('8. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn8_2').hasClass("ui-btn-a")){
                 $('#btn8_2').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn8_2').hasClass("ui-btn-g")){
                 $('#btn8_2').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn8_2').hasClass("ui-btn-a")){
                     $('#btn8_2').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn8_2').hasClass("ui-btn-g")){
                     $('#btn8_2').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    event.stopPropagation();
                    event.preventDefault();
                    $.mobile.pageContainer.pagecontainer("change", "#question9s");
                    // window.location = "quiz.html#question9"
                    // $.mobile.changePage($("#question9"), { transition: "none" });
                    return false;
                }, 2000);
                
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn8_2').hasClass("ui-btn-a")){
                     $('#btn8_2').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn8_2').hasClass("ui-btn-g")){
                     $('#btn8_2').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#question9"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question9s");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });

    $( "#btn8_3" ).click(function( event ) {
        var answer = $('#answer8-3').text();
        var trueanswer = gameArr[7][5];

        console.log('8. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn8_3').hasClass("ui-btn-a")){
                 $('#btn8_3').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn8_3').hasClass("ui-btn-g")){
                 $('#btn8_3').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn8_3').hasClass("ui-btn-a")){
                    $('#btn8_3').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn8_3').hasClass("ui-btn-g")){
                    $('#btn8_3').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    event.stopPropagation();
                    event.preventDefault();
                    $.mobile.pageContainer.pagecontainer("change", "#question9s");
                    // window.location = "quiz.html#question9"
                    // $.mobile.changePage($("#question9"), { transition: "none" });
                    return false;
                }, 2000);
            
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn8_3').hasClass("ui-btn-a")){
                     $('#btn8_3').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn8_3').hasClass("ui-btn-g")){
                     $('#btn8_3').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    };
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#question9"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question9s");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });

    $( "#btn8_4" ).click(function( event ) {
        var answer = $('#answer8-4').text();
        var trueanswer = gameArr[7][5];

        console.log('8. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn8_4').hasClass("ui-btn-a")){
                 $('#btn8_4').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn8_4').hasClass("ui-btn-g")){
                 $('#btn8_4').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn8_4').hasClass("ui-btn-a")){
                    $('#btn8_4').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn8_4').hasClass("ui-btn-g")){
                    $('#btn8_4').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    event.stopPropagation();
                    event.preventDefault();
                    $.mobile.pageContainer.pagecontainer("change", "#question9s");
                    // window.location = "quiz.html#question9"
                    // $.mobile.changePage($("#question9"), { transition: "none" });
                    return false;
                }, 2000);
        
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn8_4').hasClass("ui-btn-a")){
                     $('#btn8_4').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn8_4').hasClass("ui-btn-g")){
                     $('#btn8_4').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        // $.mobile.changePage($("#question9"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question9s");
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });
});


$( document ).on("pageinit", "#question9s", function() {

    $( "#btn9_1" ).click(function( event ) {
        var answer = $('#answer9-1').text();
        var trueanswer = gameArr[8][5];

        console.log('9. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn9_1').hasClass("ui-btn-a")){
                 $('#btn9_1').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn9_1').hasClass("ui-btn-g")){
                 $('#btn9_1').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn9_1').hasClass("ui-btn-a")){
                     $('#btn9_1').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn9_1').hasClass("ui-btn-g")){
                     $('#btn9_1').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    event.stopPropagation();
                    event.preventDefault();
                    // $.mobile.changePage($("#question7"), { transition: "none" });
                    // window.location.href="#question10s";
                    $.mobile.pageContainer.pagecontainer("change", "#question10s");
                    return false;
                }, 2000);
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn9_1').hasClass("ui-btn-a")){
                     $('#btn9_1').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn9_1').hasClass("ui-btn-g")){
                     $('#btn9_1').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#question10s");
                        // window.location ="quiz.html#question10s";
                        // $.mobile.changePage($("#question10s"), { transition: "none" });
                        // event.preventDefault();
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });

    $( "#btn9_2" ).click(function( event ) {
        var answer = $('#answer9-2').text();
        var trueanswer = gameArr[8][5];

        console.log('9. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn9_2').hasClass("ui-btn-a")){
                 $('#btn9_2').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn9_2').hasClass("ui-btn-g")){
                 $('#btn9_2').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn9_2').hasClass("ui-btn-a")){
                     $('#btn9_2').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn9_2').hasClass("ui-btn-g")){
                     $('#btn9_2').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                        event.stopPropagation();
                        event.preventDefault();
                        // window.location ="quiz.html#question10s";
                        $.mobile.pageContainer.pagecontainer("change", "#question10s");
                    return false;
                }, 2000);
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn9_2').hasClass("ui-btn-a")){
                     $('#btn9_2').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn9_2').hasClass("ui-btn-g")){
                     $('#btn9_2').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        event.stopPropagation();
                        event.preventDefault();
                        // window.location ="quiz.html#question10s";
                        $.mobile.pageContainer.pagecontainer("change", "#question10s");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });

    $( "#btn9_3" ).click(function( event ) {
        var answer = $('#answer9-3').text();
        var trueanswer = gameArr[5][5];

        console.log('9. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn9_3').hasClass("ui-btn-a")){
                 $('#btn9_3').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn9_3').hasClass("ui-btn-g")){
                 $('#btn9_3').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn9_3').hasClass("ui-btn-a")){
                    $('#btn9_3').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn9_3').hasClass("ui-btn-g")){
                    $('#btn9_3').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    event.stopPropagation();
                    event.preventDefault();
                    // $.mobile.changePage($("#question7"), { transition: "none" });
                    // window.location.href="#question10s";
                    $.mobile.pageContainer.pagecontainer("change", "#question10s");
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn9_3').hasClass("ui-btn-a")){
                     $('#btn9_3').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn9_3').hasClass("ui-btn-g")){
                     $('#btn9_3').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    };
                }else{
                    setTimeout(function(){
                        event.stopPropagation();
                        event.preventDefault();
                        // window.location ="quiz.html#question10s";
                        $.mobile.pageContainer.pagecontainer("change", "#question10s");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });

    $( "#btn9_4" ).click(function( event ) {
        var answer = $('#answer9-4').text();
        var trueanswer = gameArr[8][5];

        console.log('9. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn9_4').hasClass("ui-btn-a")){
                 $('#btn9_4').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn9_4').hasClass("ui-btn-g")){
                 $('#btn9_4').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn9_4').hasClass("ui-btn-a")){
                    $('#btn9_4').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn9_4').hasClass("ui-btn-g")){
                    $('#btn9_4').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(){
                    event.stopPropagation();
                    event.preventDefault();
                    // $.mobile.changePage($("#question7"), { transition: "none" });
                    // window.location.href="#question10s";
                    $.mobile.pageContainer.pagecontainer("change", "#question10s");
                    return false;
                }, 2000);

            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn9_4').hasClass("ui-btn-a")){
                     $('#btn9_4').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn9_4').hasClass("ui-btn-g")){
                     $('#btn9_4').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                        }, 2000);
                    }else{
                        setTimeout(function(){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                        }, 2000);
                    }
                }else{
                    setTimeout(function(){
                        event.stopPropagation();
                        event.preventDefault();
                        // window.location ="quiz.html#question10s";
                        $.mobile.pageContainer.pagecontainer("change", "#question10s");
                    }, 2000);
                }
            }
        }, 3000);
        return false;
    });
});


$( document ).on("pageinit", "#question10s", function() {
    $( "#btn10_1" ).click(function( event ) {
        var answer = $('#answer10-1').text();
        var trueanswer = gameArr[9][5];

        console.log('5. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn10_1').hasClass("ui-btn-a")){
                 $('#btn10_1').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn10_1').hasClass("ui-btn-g")){
                 $('#btn10_1').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn10_1').hasClass("ui-btn-a")){
                    $('#btn10_1').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn10_1').hasClass("ui-btn-g")){
                    $('#btn10_1').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(event){
                    // $.mobile.changePage($("#score"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    $.mobile.pageContainer.pagecontainer("change", "#score");
                    return false;
                }, 2000);
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn10_1').hasClass("ui-btn-a")){
                     $('#btn10_1').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn10_1').hasClass("ui-btn-g")){
                     $('#btn10_1').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(event){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(event){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(event){
                        // $.mobile.changePage($("#score"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#score");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
    });

    $( "#btn10_2" ).click(function( event ) {
        var answer = $('#answer10-2').text();
        var trueanswer = gameArr[9][5];

        console.log('5. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn10_2').hasClass("ui-btn-a")){
                 $('#btn10_2').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn10_2').hasClass("ui-btn-g")){
                 $('#btn10_2').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn10_2').hasClass("ui-btn-a")){
                    $('#btn10_2').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn10_2').hasClass("ui-btn-g")){
                    $('#btn10_2').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(event){
                    // $.mobile.changePage($("#score"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    $.mobile.pageContainer.pagecontainer("change", "#score");
                    return false;
                }, 2000);
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn10_2').hasClass("ui-btn-a")){
                     $('#btn10_2').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn10_2').hasClass("ui-btn-g")){
                     $('#btn10_2').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(event){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(event){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(event){
                        // $.mobile.changePage($("#score"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#score");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
    });

    $( "#btn10_3" ).click(function( event ) {
        var answer = $('#answer10-3').text();
        var trueanswer = gameArr[9][5];

        console.log('5. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn10_3').hasClass("ui-btn-a")){
                 $('#btn10_3').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn10_3').hasClass("ui-btn-g")){
                 $('#btn10_3').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn10_3').hasClass("ui-btn-a")){
                    $('#btn10_3').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn10_3').hasClass("ui-btn-g")){
                    $('#btn10_3').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(event){
                    // $.mobile.changePage($("#score"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    $.mobile.pageContainer.pagecontainer("change", "#score");
                    return false;
                }, 2000);
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn10_3').hasClass("ui-btn-a")){
                     $('#btn10_3').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn10_3').hasClass("ui-btn-g")){
                     $('#btn10_3').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(event){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(event){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(event){
                        // $.mobile.changePage($("#score"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#score");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
    });

    $( "#btn10_4" ).click(function( event ) {
        var answer = $('#answer10-4').text();
        var trueanswer = gameArr[9][5];

        console.log('5. ANSWER: ' + answer + ' CORRECT ANSWER: ' + trueanswer);
        //playAudio('/android_asset/www/music/take_chq.mp3');
        setInterval(function() {
            if ($('#btn10_4').hasClass("ui-btn-a")){
                 $('#btn10_4').removeClass("ui-btn-a").addClass("ui-btn-g");
            }else if($('#btn10_4').hasClass("ui-btn-g")){
                 $('#btn10_4').removeClass("ui-btn-g").addClass("ui-btn-a");
            }
        }, 600);

        setInterval(function() {
            if(answer == trueanswer){
                correctAnswer = correctAnswer +1;
                //playAudio('/android_asset/www/music/correct.mp3');
                if ($('#btn10_4').hasClass("ui-btn-a")){
                    $('#btn10_4').removeClass("ui-btn-a").addClass("ui-btn-e");
                }else if($('#btn10_4').hasClass("ui-btn-g")){
                    $('#btn10_4').removeClass("ui-btn-g").addClass("ui-btn-e");
                }

                progress = progress + 1;

                setTimeout(function(event){
                    // $.mobile.changePage($("#score"), { transition: "none" });
                    event.stopPropagation();
                    event.preventDefault();
                    $.mobile.pageContainer.pagecontainer("change", "#score");
                    return false;
                }, 2000);
            }else{
                //playAudio('/android_asset/www/music/false.mp3');
                if ($('#btn10_4').hasClass("ui-btn-a")){
                     $('#btn10_4').removeClass("ui-btn-a").addClass("ui-btn-d");
                }else if($('#btn10_4').hasClass("ui-btn-g")){
                     $('#btn10_4').removeClass("ui-btn-g").addClass("ui-btn-d");
                }

                life = life - 1;
                progress = progress + 1;

                if (life == 2){
                    $( ".heart1" ).remove();
                }else if (life == 1) {
                    $( ".heart2" ).remove();
                }else{
                    $( ".heart3" ).remove();
                }

                if(life < 1) {
                    $('#your_score_2').text(correctAnswer);

                    if (correctAnswer < 5){
                        setTimeout(function(event){
                            $.mobile.changePage($("#failed"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }else{
                        setTimeout(function(event){
                            $.mobile.changePage($("#score"), { transition: "none" });
                            event.preventDefault();
                            return false;
                        }, 2000);
                    }
                }else{
                    setTimeout(function(event){
                        // $.mobile.changePage($("#score"), { transition: "none" });
                        event.stopPropagation();
                        event.preventDefault();
                        $.mobile.pageContainer.pagecontainer("change", "#score");
                        return false;
                    }, 2000);
                }
            }
        }, 3000);
    });
});