
var gameArr = new Array();
var score = 0;
var indexquestion = 0;
var correctAnswer = 0;
var questionArray = [];

var life = 3;
var progress =0;

var my_media = null;


var db = window.openDatabase("crimedb", "1.0", "crime_app", 1000000);

document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
    if(db != null && db != undefined && db != ""){
        db.transaction(populatedb, errorCB, successCB);            
    }
}


function playAudio(url) {
    // Play the audio file at url
        my_media = new Media(url,
        // success callback
        function () {
            console.log("playAudio():Audio Success");
        },
        // error callback
        function (err) {
            console.log("playAudio():Audio Error: " + err);
        }
    );
    // Play audio
    my_media.play();
}

function stopAudio() {
    if (my_media) {
        my_media.stop();
    }
}



function viewAuthor(){
    //playAudio('/android_asset/www/music/splash.mp3');
    $.mobile.changePage($("#author"));
}

function viewGameInfo(){
    //playAudio('/android_asset/www/music/splash.mp3');
    $.mobile.changePage($("#gameinfo"));
}

function viewCategory(){
    //playAudio('/android_asset/www/music/fiftyfifty.mp3');
    $.mobile.changePage($("#category"));
}


function closeApp(){
    console.log('closeApp');
    //stopAudio();
    navigator.app.exitApp();

} 

function shuffle(array) {
    var tmp, current, top = array.length;
    if(top) while(--top) {
        current = Math.floor(Math.random() * (top + 1));
        tmp = array[current];
        array[current] = array[top];
        array[top] = tmp;
    }
    return array;
}


function errorCB(err) {
    console.log("Error processing SQL: "+err.code + ' ' + err.message);
}

function successCB() {
    console.log("success!");
}



$(document).on('click' , '.lstCategory' , function(){
    var idAttr = $(this).attr('data-id');
    var c_name = $(this).find("h5").text();
    
    // alert('c_name: ' + c_name);

    localStorage.setItem('c_id',idAttr);
    localStorage.setItem('c_name',c_name);   
    // $.mobile.changePage($("#quiz1")); 
     window.location.href="quiz.html"
   return false; // to prevent the default action of the link also prevents bubbling 
});


function retryQuiz(){
    correctAnswer = 0;
    window.location.href = 'quiz.html';
}




















function populatedb(tx){

	tx.executeSql('DROP TABLE IF EXISTS category');
    tx.executeSql('DROP TABLE IF EXISTS quiztbl');

    tx.executeSql('CREATE TABLE IF NOT EXISTS category (c_id INTEGER PRIMARY KEY AUTOINCREMENT, c_name TEXT,images VARCHAR(45))');
    tx.executeSql('CREATE TABLE IF NOT EXISTS quiztbl (ID INTEGER PRIMARY KEY AUTOINCREMENT, c_id INTEGER REFERENCES category(c_id) ON DELETE CASCADE ON UPDATE CASCADE, question text, qanswer1 text, qanswer2 text, qanswer3 text,qanswer4 text, answer text)');


    tx.executeSql('INSERT INTO category(c_name,images) VALUES ("MOST WANTED LIST","mw.png")');
    tx.executeSql('INSERT INTO category(c_name,images) VALUES ("CRIMES","crime.png")');
    tx.executeSql('INSERT INTO category(c_name,images) VALUES ("POLICE RANKS","rank.png")');
    tx.executeSql('INSERT INTO category(c_name,images) VALUES ("ANCIENT WEAPONS","ancient.png")');
    tx.executeSql('INSERT INTO category(c_name,images) VALUES ("MODUS OPERANDI","modus.png")');
    tx.executeSql('INSERT INTO category(c_name,images) VALUES ("HUMAN RIGHTS ABUSES","human.png")');
    tx.executeSql('INSERT INTO category(c_name,images) VALUES ("KNOWING TRAFFIC SIGNS","traffic.png")');
    tx.executeSql('INSERT INTO category(c_name,images) VALUES ("EXPLOSIVE WEAPONS","bomb.png")');


    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (1, "Was a Filipino film actor and convicted murderer.The son of actors Miguel Anzures and Rosa Aguirre, Anzures had starred in pre-World War II films as a child actor. After the war, he was paired with Lilian Velez in three LVN films, Binibiro Lamang Kita, Ang Estudyante, and Sa Kabukiran.", "Narding Anzures", "Leo Echegaray", "Victor Castigador", "Nardong Putik","Narding Anzures")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (1, "Was a Filipino gangster turned folk hero. An hoodlum from Cavite province, Putik credited his ability to survive and escape numerous ambushes and gunfights to his anting-anting (amulet).", "Narding Anzures", "Leo Echegaray", "Nardong Putik", "Romeo Jalosjos","Nardong Putik")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (1, "One of five prime suspects in the Vizconde Massacre case. He was eventually acquitted and freed by the Supreme Court of the Philippines after spending 15 years in jail.", "Andrew Cunanan", "Hubert Webb", "Antonio Sanchez", "Leo Echegaray","Hubert Webb")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (1, "Was the first Filipino to be meted the death penalty (by lethal injection) in 1999 after its reinsatement in the Philippines in 1995, and 23 years after the last execution was carried out under Philippine law. His death sparked debates about the legality and morality of the death penalty in the Philippines.", "Roberto S. Buenaventura", "Leo Echegaray", "Andrew Cunanan", "Rolito Go","Leo Echegaray")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (1, "Is a subdivision owner and is on the Philippine National Polices Most Wanted list for robbery with homicide. He was born September 4, 1953, the son of international jeweler Fe S. Panlilio. He is described as 5 feet 8 inches tall, with brown eyes and fair complexion.", "Roberto S. Buenaventura", "Corey Dickpus", "Allan Estrada", "Jose Ma. Bong Panlilio","Jose Ma. Bong Panlilio")');    
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (1, "Is on the Philippine National Polices most wanted list for the crime of kidnap for ransom. He was last seen in Bgy. Suplang, Tanauan, Batangas. The reward for those who can give information regarding his whereabout is one million pesos.", "Allan Estrada", "Aldrin Fajardo", "Harold Fajardo", "Rolando Fajardo","Rolando Fajardo")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (1, "Is a former vice mayor and politician and the younger brother of Mayor Johnny Dickpus of Lubuagan, Kalinga. He has been charged with multiple murder. He was the alleged mastermind behind the bloody attack against members of the Special Action Forces in 2006, resulting in the death of three policemen, including an office.", "Corey Dickpus", "Sheik Reuben Omar Lavilla", "Ignacio Monteson Jr.", "Jaime Moog","Corey Dickpus")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (1, "Alias Ka Drilon was considered the number one most wanted criminal in Negros Oriental for the crime of robbery with multiple homicide. A suspected New Peoples Army member, he allegedly murdered four employees of Santa Catalina local government while riding the local governments ambulance in May 2002.", "Raul Villar", "Amado Bucala", "Rafael Cardeño", "Jose Pacheco Jr.","Raul Villar")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (1, "Alias Juancho, is on the National Bureau of Investigations Most Wanted list. He received the death sentence for the crime of rape. Manalo remains at large", "Juan Manalo", "Joel P. Marcelo", "Nestor Orollo", "Eleazar Perez","Juan Manalo")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (1, "A former Special Collection Officer at the Bureau of Internal Revenue, is on the National Bureau of Investigation Most Wanted list for the crime of plunder.", "Joseph Gan", "Rolando Gobiana", "Juan Manalo", "Joel P. Marcelo","Joel P. Marcelo")');
    
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (2, "Any person who, owing allegiance to (the United States or) the Government of the Philippine Islands, not being a foreigner, levies war against them or adheres to their enemies, giving them aid or comfort within the Philippine Islands or elsewhere, shall be punished by reclusion temporal to death and shall pay a fine not to exceed P20,000 pesos.", "Crimes Against National Security", "Crimes Against Public Order", "Crimes Relative to Opium & Other Prohibited Drugs","Crimes Against Public Morals","Crimes Against National Security")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (2, "Using forged signature or counterfeit seal or stamp","Forgeries","Penalties","Extinction of Criminal Liability","Civil Liabilities","Forgeries")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (2, "Possession, preparation and use of prohibited drugs and maintenance of opium dens.", "Crimes Committed By Public Officers", "08. Crimes Against Persons", "09. Crimes Against Personal Liberty & Security", "Crimes Relative to Opium and other prohibited drugs","Crimes Relative to Opium and other prohibited drugs")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (2, "Any person who, with intent to gain, shall take any personal property belonging to another, by means of violence or intimidation of any person, or using force upon anything shall be guilty of robbery.", "Crimes Against Persons","Crimes Against Personal Liberty & Security", "Crime Against Property", "Crimes Against Chastity","Crime Against Property")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (2, "Any private individual who shall kidnap or detain another, or in any other manner deprive him of his liberty, shall suffer the penalty of reclusion perpetua to death:", "Crimes Against Persons", "Crimes Against Personal Liberty & Security", "Crime Against Property", "Crimes Against Chastity","Crimes Against Personal Liberty & Security")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (2, "A libel is public and malicious imputation of a crime, or of a vice or defect, real or imaginary, or any act, omission, condition, status, or circumstance tending to cause the dishonor, discredit, or contempt of a natural or juridical person, or to blacken the memory of one who is dead.", "Crime Against Property", "Crimes Against Chastity", "Crimes Against the Civil Status of Persons", "Crime Against Honor","Crime Against Honor")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (2, "The abduction of any woman against her will and with lewd designs shall be punished by reclusion temporal.", "Crime Against Property", "Crimes Against Chastity", "Crimes Against the Civil Status of Persons","Crime Against Honor","Crimes Against the Civil Status of Persons")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (2, "ARBITRARY DETENTION OR EXPULSION, VIOLATION OF DWELLING, PROHIBITION, INTERRUPTION, AND DISSOLUTION OF PEACEFUL MEETINGS AND CRIMES AGAINST RELIGIOUS WORSHIP", "Crimes Against National Security & the Law of Nations", "Crimes Against the Fundamental Laws of the State", "Crimes Against Public Order", "Crimes Relative to Opium & Other Prohibited Drugs","Crimes Against the Fundamental Laws of the State")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (2, "Any person who, by direct provision of the law, popular election or appointment by competent authority, shall take part in the performance of public functions in the Government of the Philippine Islands, of shall perform in said Government or in any of its branches public duties as an employee, agent or subordinate official, of any rank or class, shall be deemed to be a public officer.", "Crimes Relative to Opium & Other Prohibited Drugs", "Crimes Against Public Morals", "Crimes Committed By Public Officers", "Crimes Against Persons","Crimes Committed By Public Officers")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (2, "Adultery is committed by any married woman who shall have sexual intercourse with a man not her husband and by the man who has carnal knowledge of her knowing her to be married, even if the marriage be subsequently declared void.", "Crimes Against Personal Liberty & Security", "Crime Against Property", "Crimes Against Chastity", "Crimes Against the Civil Status of Persons","Crimes Against Chastity")');
    
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (3, "two crossed batons below a pip", "Deputy Commissioner", "Assistant Commissioner", "Commander", "Superintendent","Deputy Commissioner")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (3, "two crossed batons","Deputy Commissioner", "Assistant Commissioner", "Commander", "Superintendent","Assistant Commissioner")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (3, "three pips in a triangular pattern below a crown", "Deputy Commissioner", "Assistant Commissioner", "Commander", "Superintendent","Commander")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (3, "one pip below a crown", "Deputy Commissioner", "Assistant Commissioner", "Commander", "Superintendent","Superintendent")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (3, "three pips", "Deputy Commissioner", "Assistant Commissioner", "Commander", "Inspector","Inspector")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (3, "three chevrons, crown and surmounted by a laurel wreath", "Senior Constable", "Constable", "Probationary Constable", "Senior Sergeant","Senior Sergeant")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (3, "three chevrons", "Senior Constable", "Constable", "Senior Sergeant", "Sergeant","Sergeant")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (3, "two bars above two chevrons", "Leading Senior Constable", "Constable", "Probationary Constable", "Senior Sergeant","Leading Senior Constable")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (3, "two chevrons", "Senior Constable", "Constable", "Probationary Constable", "Leading Senior Constable","Senior Constable")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (3, "one chevron", "Senior Constable", "Constable", "Probationary Constable", "Leading Senior Constable","Constable")');

    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (4, "Is a folding pocket knife with two handles counter-rotating around the tang such that, when closed, the blade is concealed within grooves in the handles. It is sometimes called a Batangas knife, after the Tagalog province of Batangas in the Philippines, where it is traditionally made.", "Butterfly knife", "Balisword", "Barong (knife)", "Bolo knife","Butterfly knife")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (4, "An exceptionally large balisong. Similar to a balisong, two hilts cover the blade of a balisword. These handles fold away from the blade to expose it. The standard length of an open sword is around 37 inches(93.98 cm) long. A normal blade measures at around 17 inches (43.18 cm) long, with a set of folding hilts about 20 inches (50.49 cm) long", "Butterfly knife", "Balisword", "Barong (knife)", "Bolo knife;","Balisword")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (4, "Sting ray tail is a type of Filipino whiplike weapon. It is most known for fighting and warding off aswangs and other similar mythical creatures in Philippine folklore. In actual combat, a Buntot Pagi is often used with a balaraw or a short knife or sword.", "Balisword", "Barong (knife)", "Bolo knife", "Buntot pagi","Buntot pagi")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (4, "Literally rice leaf in Tagalog, is a single-edged sword from the Philippines, specifically the Southern Tagalog provinces of Batangas and Mindoro. The swords name could either be a reference to the similarity of its shape to the leaves of rice or to local green snakes dahong palay, purported to be extremely venomous.", "Barong (knife)", "Bolo knife", "Buntot Pagi", "Dahong palay","Dahong palay")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (4, "Is a large rectangular shield used by the natives in the Philippines. The shield is made of hardwood and is decorated with elaborate carvings.[1] The wood comes from native trees such as the dapdap, polay and sablang.[2] It was widely used throughout the archipelago for warfare", "Floro MK-9", "Floro PDW", "Gunong", "Kalasag","Kalasag")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (4, "Is a knife from Mindanao, the Philippines. It is essentially a diminutive form of the larger kalis or kris. The gunong serves both as a utility knife and as a thrusting weapon used for close quarter fighting - usually as a last defense.", "Gunong", "Kalasag", "Buntot Pagi", "Floro MK-9","Gunong")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (4, "Is a thick, leaf-shaped, single-edged blade sword. It is a weapon used by Islamic tribes in the Southern Philippines", "Butterfly knife", "Balisword", "Barong (knife)", "Bolo knife","Barong (knife)")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (4, "Is a staff or spear used as a weapon or tool by natives of the Philippines. It also called bangkaw, sumbling or palupad in the islands of Visayas and Mindanao. Sibat are typically made from rattan, either with a sharpened tip or a head made from metal.", "Gunong", "Kalasag", "Buntot Pagi", "Sibat","Sibat")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (4, "Is a large, forward-curved sword, used by certain ethnic groups in the southern Philippines. It can range in size from 2 to 4 feet and can be held with one or both hands, delivering a deep, meat cleaver-like cut.", "Kalasag", "Buntot Pagi", "Sibat", "Panabas","Panabas")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (4, "Is a shield of Moro people, Philippines. It is made of tightly woven rattan. Its origin is unsure: various scholars suggest it comes from Maranaos, Chinese, Moluccan, or Spanish.", "Taming (shield)", "Sibat", "Panabas", "Floro MK-9","Taming (shield)")');

    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (5, "Suspects are typically well-dressed, mild-mannered, and project an aura of legitimate businessman or an affluent matron; complete with jewelry, attaché case and other props to appear and look wealthy. The perpetrator moves closer to the would-be victim and waits patiently until the victim is engrossed in a serious conversation with a companion or leaves his or her bags and other belongings unattended. In a swift motion, the perpetrator takes the unattended bag or belongings and casually leaves the place", "Salisi Gang", "Tutok-Kalawit Gang", "Ativan Gang", "Ipit Gang","Salisi Gang")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (5, "Operate in groups of four or five. Gang members shove or push a prospective victim to distract him or her, while their accomplice picks his pocket. In jeepneys and buses, suspects squeeze-in and distract their victims while their accomplice snatches the victim’s wallet and/or mobile phone", "Salisi Gang", "Tutok-Kalawit Gang", "Ativan Gang", "Ipit Gang","Ipit Gang")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (5, "Commonly victimize foreigners who roam alone in public places. The group is usually composed of three (3) to four (4) males or females who befriend the would-be victim. After gaining trust and confidence, the victim will be taken for a ride to other tourist sites and during meal time, the victim will be brought to their house usually situated in a squatter colony where the victim will be treated for lunch, snacks, or dinner. The served drink is spiked with Ativan – a powerful anti-depressant/sleeping pill. Even before finishing the drink, the victim will succumb to a deep sleep and while sleeping, will be stripped of his cash and valuables and will be brought out of the house and left at a completely random location", "Salisi Gang", "Tutok-Kalawit Gang", "Ativan Gang", "Ipit Gang","Ativan Gang")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (5, "Is a transaction scam principally involving a supposed bundle (budol) of cash that is actually padded inside with sheets of paper cut in the size of money. Only the exposed sides however are real money, everything in between are plain paper cuttings", "Tutok-Kalawit Gang", "Ativan Gang", "Ipit Gang", "BUDOL-BUDOL GANG","BUDOL-BUDOL GANG")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (5, "Perpetrators of this crime usually target victims in crowded areas. A man/woman/child pretending to be lost or selling an item approaches the victim to distract his/her attention. An accomplice slashes the bag/pocket of the victim who is busy being distracted by another suspect. All money and goods are stolen.", "Ativan Gang", "Ipit Gang", "BUDOL-BUDOL GANG", "Laslas Bag/Laslas Bulsa","Laslas Bag/Laslas Bulsa")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (5, "Usually involves three (3) perpetrators. The trio uses a taxi cab spray painted with a different name and sporting stolen or fake license plates. The driver usually drives around looking for a potential victim who is hailing a taxi cab. Unknown to the victim, the locking mechanisms of both rear doors are not working. The driver then drives the cab to a pre-arranged area, usually a dimly lit street or highway, and slows down pretending he has engine/mechanical trouble.", "Tutok-Kalawit Gang", "Ativan Gang", "Ipit Gang", "Ipit Taxi Gang","Ipit Taxi Gang")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (5, "Thieves typically work in pairs. Spotting a potential victim driving a car with unlocked doors, a pair will force their way into an occupied parked car or a vehicle stopped at an intersection. Other times, using a car of their own, the pair will force the victim to maneuver his or her vehicle off the road. One of the attackers will force the victim to open his door. The attacker pushes the victim to the front passenger seat, drives the car to a deserted area, and robs the victim. Sometimes, the attackers also steal the car.", "Bukas Kotse Gang", "Dura Boys", "Akyat-Bahay Gang", "Estribo Gang","Bukas Kotse Gang")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (5, "This tactic is usually carried out by a group of three. The first member informs the victim that a man/woman has spit on her sleeve and back. The victim will be distracted trying to wipe the spit on her sleeve while one of the other members of the gang steals the victim’s valuables, usually a wallet or a mobile phone", "Dura Boys", "Akyat-Bahay Gang", "Estribo Gang", "Pitas Gang","Dura Boys")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (5, "Another variant occurs when a group of thieves grab the ears of women and young girls and steal their earrings or snatch their bracelets from their wrists", "Dura Boys", "Akyat-Bahay Gang", "Estribo Gang", "Pitas Gang","Pitas Gang")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (5, "Members of this gang drop coins or small bills near their victim. While the victim helps to scoop up the money, other gang members start robbing the victim. In most instances, a gang member blends with the crowd and serves as lookout or “stopper,” when someone tries to run after his companions", "Akyat-Bahay Gang", "Estribo Gang", "Pitas Gang", "Laglag-Barya Gang","Laglag-Barya Gang")');

    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (6, "Occurs when a person purposely, knowingly, recklessly, or negligently causes the death of another.", "Criminal Homicide", "Extrajudicial Killings‎", "Crimes against humanity‎", "Discrimination","Criminal Homicide")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (6, "Is the trade in humans, most commonly for the purpose of sexual slavery, forced labor or commercial sexual exploitation for the trafficker or others", "Crimes against humanity‎", "Discrimination", "Human Tranfficking", "Child Labour","Human Trafficking")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (6, "Refers to the employment of children in any work that deprives children of their childhood, interferes with their ability to attend regular school, and that is mentally, physically, socially or morally dangerous and harmful.", "Crimes against humanity‎", "Discrimination", "Human Trafficking", "Child Labour","Child Labour")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (6, "Also known as marriage by abduction or marriage by capture, is a practice in which a man abducts the woman he wishes to marry", "Human Trafficking", "Child Labour", "Bride Kidnapping", "Massacres","Bride Kidnapping")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (6, "The systematic use of violence (terror) as a means of coercion for political purposes.", "Terrorism", "Torture", "Wars", "Extra Judicial Killings","Terrorism")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (6, "The systematic mistreatment of an individual or group by another individual or group", "Persecution", "Slavery", "Police Brutality", "Massacres","Persecution")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (6, "The act of deliberately inflicting severe physical or psychological pain and possibly injury to a person (or animal), usually to one who is physically restrained or otherwise under the torturers control or custody and unable to defend against what is being done to them", "SLAVERY", "TORTURE", "BRUTALITY", "HARRASSMENT","TORTURE")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (6, "A system under which people are treated as property to be bought and sold, and are forced to work.", "SLAVERY", "BRUTALITY", "TORTURE", "HUMAN TRAFICKING","SLAVERY")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (6, "A specific incident in which a military force, mob, or other group kill many people—and the perpetrating party is perceived as in total control of force while the victimized party is perceived as helpless or innocent.", "MASSACRE", "TORTURE", "WAR", "LYNCHINGS","MASSACRE")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (6, "The homicide of a member of a family or social group by other members, due to the perpetrators belief that the victim has brought shame or dishonor upon the family or community, usually for reasons such as refusing to enter an arranged marriage, being in a relationship that is disapproved by their relatives, having sex outside marriage, becoming the victim of rape, dressing in ways which are deemed inappropriate, or engaging in homosexual relations.", "HONOR KILLING", "WAR", "TORTURE", "LYNCHINGS","HONOR KILLING")');

    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (7, "You cannot park in this area at any time.", "NO PARKING SIGN", "DIVIDED ROAD SIGN", "2 WAY TRAFFIC SIGN", "ONE WAY SIGN","NO PARKING SIGN")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (7, "This sign warns you that you will have a divided road ahead.", "NO PARKING SIGN", "DIVIDED ROAD SIGN", "DIVIDED HI-WAY SIGN", "ONE WAY SIGN","DIVIDED HI-WAY SIGN")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (7, "Do not go on to this road.", "DO NOT ENTER SIGN", "ONE WAY SIGN", "NO PARKING SIGN", "OFF LIMIT SIGN","DO NOT ENTER SIGN")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (7, "You cannot park in this area unless you have a handicapped sticker or license plate.", "DO NOT ENTER SIGN", "ONE WAY SIGN", "NO PARKING SIGN", "HANDICAPPED SIGN","HANDICAPPED SIGN")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (7, "The road will narrow ahead from two lanes to one lane.", "LANE REDUCTION SIGN", "STOP LIGHT SIGN", "ONE WAY SIGN", "OFF LIMIT SIGN","LANE REDUCTION SIGN")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (7, "You must turn left if you are in this lane.", "LANE REDUCTION SIGN", "STOP LIGHT SIGN", "ONE WAY SIGN", "Lt TURN ONLY SIGN","Lt TURN ONLY SIGN")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (7, "The right lane will end soon, so you need to move into the left lane.", "STOP LIGHT SIGN", "ONE WAY SIGN", "Lt TURN ONLY SIGN", "MERGE LEFT SIGN","MERGE LEFT SIGN")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (7, "You cannot make a u-turn here.", "ONE WAY SIGN", "Lt TURN ONLY SIGN", "MERGE LEFT SIGN", "NO U-TURN SIGN","NO U-TURN SIGN")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (7, "You may only drive in the direction of the arrow.", "ONE WAY SIGN", "Lt TURN ONLY SIGN", "MERGE LEFT SIGN", "NO U-TURN SIGN","ONE WAY SIGN")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (7, "You must lower your speed ahead.", "LANE REDUCTION SIGN", "STOP LIGHT SIGN", "ONE WAY SIGN", "REDUCE SPEED SIGN","REDUCE SPEED SIGN")');

    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (8, "A type of explosive weapon intended to travel through the air with predictable trajectories, usually designed to be dropped from an aircraft", "Aerial bomb", "Anti-personnel mine", "Anti-tank mine", "Artillery","Aerial bomb")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (8, "A form of land mine designed for use against humans, as opposed to anti-tank mines, which are designed for use against vehicles", "Aerial bomb", "Anti-personnel mine", "Anti-tank mine", "Artillery","Anti-personnel mine")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (8, "Is a type of land mine designed to damage or destroy vehicles including tanks and armored fighting vehicles.", "Aerial bomb", "Anti-personnel mine", "Anti-tank mine", "Artillery","Anti-tank mine")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (8, "Refers to an engine of war that uses stored energy, whether mechanical, chemical, or electromagnetic, to project munitions far beyond the effective range of personal weapons", "Aerial bomb", "Anti-personnel mine", "Anti-tank mine", "Artillery","Artillery")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (8, "An explosive device, concealed under or on the ground and designed to destroy or disable enemy targets as they pass over or near the device", "Anti-personnel mine", "Anti-tank mine", "Artillery", "Land Mine","Land Mine")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (8, "An explosive charge placed within one or several connected tubes", "Bangalore torpedo", "Blast radius", "Bomb", "Bombardment","Bangalore torpedo")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (8, "Any of a range of explosive weapons that only rely on the exothermic reaction of an explosive material to provide an extremely sudden and violent release of energy (an explosive device).", "Bangalore torpedo", "Blast radius", "Bomb", "Bombardment","Bomb")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (8, "An attack by artillery fire directed against fortifications, combatants, or towns and buildings", "Bangalore torpedo", "Blast radius", "Bomb", "Bombardment","Bombardment")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (8, "Was a concept that was invented by the British aeronautical engineer Barnes Wallis early in World War II and subsequently developed and used during the war against strategic targets in Europe", "Cluster munition", "Controlled mines", "Earthquake bomb", "Explosive harpoon","Earthquake bomb")');
    tx.executeSql('INSERT INTO quiztbl (c_id, question, qanswer1, qanswer2, qanswer3,qanswer4, answer) VALUES (8, "Was a circuit fired weapon used in coastal defenses with ancestry going back to 1805 when Robert Fulton termed his underwater explosive device a torpedo:", "Cluster munition", "Controlled mines", "Earthquake bomb", "Explosive harpoon","Controlled mines")');

}